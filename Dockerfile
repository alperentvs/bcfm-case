FROM java:8-jdk-alpine as build
WORKDIR /tmp
COPY ./target/helloworld-0.0.1-SNAPSHOT.jar .

FROM java:8-jre-alpine
WORKDIR /tmp
COPY --from=build /tmp/helloworld-0.0.1-SNAPSHOT.jar .
RUN sh -c "touch helloworld-0.0.1-SNAPSHOT.jar"
ENTRYPOINT ["java","-jar","helloworld-0.0.1-SNAPSHOT.jar"]
