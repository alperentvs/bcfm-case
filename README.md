# CLOUD NATIVE ENGINEER ASSOCIATE CASE STUDY

## YAPILAN İŞLEMLER
* Öncelikle verilen adresten ilgili repoyu kendi localime clone ettim.
* Sonrasında clone ettiğim repodaki dosyaları maven aracını kullanalarak testten geçirerek, paket haline getirdim bunun için aşağıdaki komutları kullandım:
    - ``mvn clean`` --> Projeyi temizler ve önceki derleme tarafından oluşturulan tüm dosyaları kaldırır.
    - ``mvn compile`` --> Projenin kaynak kodunu compile eder.
    - ``mvn test`` --> Proje için testleri çalıştırır.
    - ``mvn package`` --> Projenin dağıtılabilir bir biçime dönüştürülmesi için JAR veya WAR dosyası oluşturur.
    - ``mvn install`` --> Paketlenmiş JAR/WAR dosyasını yerel depoya dağıtır.
* Bu komutları kullandığım esnada bazı hatalar ile karşılaştım ve şu şekilde sorunları giderdim:
    * maven için build sonrası jar olusmadigi icin pom icine "<package>jar</package>" kısmı eklendi.
    * maven test asamasında testten geçemediği icin main altında controller/homecontroller.java dosyası test case'ine uygun şekilde değiştirildi. Hatayı şu şekilde paylaşabilirim:
    ![test](images/test-hata.png)

* Bu işlemlerin ardından package haline getirilmiş uygulamayı Dockerfile kullanarak çalışır halde dockerize ettim.
* Dockerize edilmiş uygulamayı Gitlab CI/CD toolunu kullanarak build ve deploy aşamalarından oluşan pipeline adımlarını oluşturdum.
* Bu aşama için gitlab-runner kullandım ve bunu size iletmiş olduğum vm üzerinde gerçekleştirdim.
* Containerların deploy edildiği ortamda nginx üzerinde proxy kullanarak containerları dış dünyaya açtım. 
* **Görev 4** için gerekli bilgileri aşağıda veriyorum:

## BLUE/GREEN DEPLOYMENT
* Blue/Green Deploymentin bize sağladığı faydaların başında sürekli ve risksiz güncellemeler yer almaktadır. Bunu "Blue" ve "Green" olarak adlandırılan birbirine özdeş olan iki ortamda yapmaktadır. Bu sayede kullanıcılar bir ortama erişirken diğer ortamda güncellemeler yapılmaktadır. Blue ve Green sırasıyla, her döngüde boş olan ve kullanılmayan ortama yeni sürümleri deploy ederek testler yapar ve son olarakta tüm kullanıcıları yapılan değişikliklerden yararlanması için bu ortama alır. Bunun sayesinde:

	- Gerçek bir prod ortamda testler yapılmış olur
	- Kullanıcılar bu versiyon değişikliği sırasında herhangi bir kesinti yaşamaz
	- Herhangi bir kesinti veya sorunda diğer ortama dönülerek sorun giderilebilir. 
	
* Blue/Green deployment altyapısını yönetme şeklimiz hangi ortamda çalıştığımıza bağlıdır. Eğer bare metal servers(fiziksel sunucu) kullanıyorsak çoğu zaman bir sistem boşta duracaktır. Fakat pratikte IaC kullanarak cloud tarafında isteğe bağlı kaynak sağlamak daha uygundur. Örnek verecek olursak deploymente başlamadan önce sanal makineleri başlatabilir ve konteynerleri çalıştırabilir, network ve servisleri yapılandırabiliriz. Kullanıcılar yeni versiyona geçtikten sonra eski ortamı kapatabiliriz. Bunu "Kubernetes" ile yapabiliriz. Bahsetmiş olduğum bütün bu adımları ve aşamaları tek bir araç(k8s) kullanarak yönetebiliriz. 

* Oluşturmuş olduğum container ortamı için kubernetes kullanacak olursam şu şekilde bir açıklama yapabilirim. Şuanki oluşturmuş olduğum uygulamamın adı MyApp olsun ve V1 sürümüne sahip olduğunu düşünelim. Bu uygulamam "Blue" ortamında çalışıyor. Kubernetes ortamında uygulamamı deployments ve podlar ile çalıştırıyorum.

    ![blue-deployment](images/blue-deployment.jpeg)

* Bir süre sonra uygulamamızın yeni versiyonu olan v2 yi hazır hale getiriyoruz. Bunun sonucunda "Green" ortamı olan yeni ortamımızı yaratıyoruz. Kubernetesin bize sağladığı kolaylık sayesinde yeni deployments objemizi bildirmemiz gerekiyor ve gerisini kubernetes platformu hazır hale getiriyor. Blue ortamı bunlardan etkilenmeden çalışmasına devam ediyor, kullanıcıların Green ortamından ve değişiklikten haberdar değiller. Blue'ye gelen trafiği Green'e çevirene kadar herhangi bir değişiklik göremeyecekler.

    ![create-green](images/create-green.jpeg)

* Trafiği Blue'den Green'e aldıktan sonra Blue ortamında herhangi bir risk durumu olmadan test yapma şansına sahibiz. 

    ![v2-released](images/v2-released.jpeg)

* Kullanıcıları Blue'den Green'e taşıdıktan ve sonuçtan memnun kaldıktan sonra, gereksiz kaynakları ortadan kaldırmak için Blue ortamını kaldırabiliriz. 

    ![deleted-blue](images/deleted-blue.jpeg)

* Sonuç olarak Blue/Green ortamında aynı anda iki deploymenti ve networku yönetmeliyiz. Bu durum biraz karışık gibi dursada Kubernetes platformu işleri çok daha kolay hale getiriyor.

* Oluşturduğum container ortamı için bu konuya değinecek olursam, bu ortamı özetle şu şekilde Blue/Green deployment stratejisine göre deploy edebilirim: İkinci bir docker-compose.yml oluştururuz. Image ismini ve portunu değiştirip ayağa kaldırdığımızda iki tane container ayakta mevcut olarak bulunacaktır. Bunlardan birisi v1 verirken diğeride v2 yi verecektir. v1 sürümünden v2 sürümüne geçmek istediğimde proxy ile trafiği v2 ye çekebilirim, bu esnada blue hala ayakta olacaktır fakat kullanıcılar burdaki değişikliklerden haberdar olmayacaktır.
